//
//  AppDelegate.m
//  WeddingInvitation
//
//  Created by yoseop on 2014. 10. 21..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // 기본 네비게이션 컬러
    

    UIColor *navigationColor = [UIColor colorWithRed:27/255.0 green:30/255.0 blue:29/255.0 alpha:1.0f];
    // 네비게이션 컬러
    [[UINavigationBar appearance] setBarTintColor:navigationColor];
    
    
    UIColor *titleColor = [UIColor colorWithRed:255.0f/255.0f green:194.0f/255.0f blue:54.0f/255.0f alpha:1.0f];
    
    // 타이틀 컬러
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: titleColor}];
    
    // 바버튼 아이템 컬러
    [[UINavigationBar appearance] setTintColor:titleColor];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
