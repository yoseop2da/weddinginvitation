//
//  InvitationViewController.m
//  WeddingInvitation
//
//  Created by yoseop on 2014. 10. 22..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "InvitationViewController.h"

@interface InvitationViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong) UIActionSheet *actionSheet;

@end

@implementation InvitationViewController
{
    BOOL _anyImage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"청첩장";
    
    [self barButtonItemSetting];
    [self defaultValueSetting];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)barButtonItemSetting
{
    UIBarButtonItem *nextButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"다음으로" style:UIBarButtonItemStylePlain target:self action:@selector(nextButtonTouched)];
    UIBarButtonItem *cameraButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(cameraButtonTouched)];
    self.navigationItem.rightBarButtonItems = @[nextButtonItem,cameraButtonItem];
}

- (void)defaultValueSetting
{
    if ([Couple sharedInstance].invitation) {
        self.imageView.image = [Couple sharedInstance].invitation;
        _anyImage = YES;
    }else{
        _anyImage = NO;
    }
}

- (void)nextButtonTouched
{
    if (_anyImage) {
        [Couple sharedInstance].invitation = self.imageView.image;

        UIViewController *weddingPicturesViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WeddingPicturesViewController"];
        [self.navigationController pushViewController:weddingPicturesViewController animated:YES];
    }else{
        [self showAlertWithMessage:@"청첩장이 비어있습니다."];
    }
}

- (void)showAlertWithMessage:(NSString *)message
{
    ButtonItem *okButton = [ButtonItem itemWithLabel:@"  청첩장 추가하기  "];
    [okButton setAction:^{
        [self cameraButtonTouched];
    }];
    ButtonItem *cancelButton = [ButtonItem itemWithLabel:@"Skip"];
    [cancelButton setAction:^{
        UIViewController *weddingPicturesViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WeddingPicturesViewController"];
        [self.navigationController pushViewController:weddingPicturesViewController animated:YES];
    }];
    
    [[[UIAlertView alloc] initWithTitle:self.title message:message cancelButtonItem:cancelButton otherButtonItems:okButton, nil] show];
}

- (void)cameraButtonTouched
{
    _actionSheet = [[UIActionSheet alloc] initWithTitle:@"청첩장"
                                               delegate:self
                                      cancelButtonTitle:@"취소"
                                 destructiveButtonTitle:nil
                                      otherButtonTitles:@"사진 찍기", @"앨범에서 가져오기", nil];
    [_actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
#define TakeAPhoto 0
#define SelectPhoto 1
    
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    switch (buttonIndex) {
        case TakeAPhoto:
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self.navigationController presentViewController:imagePickerController animated:YES completion:nil];
            break;

        case SelectPhoto:
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self.navigationController presentViewController:imagePickerController animated:YES completion:nil];
            break;
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:actionSheet.title]];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
{
    _anyImage = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.imageView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    self.imageView.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
}




@end
