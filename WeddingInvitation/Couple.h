//
//  Couple.h
//  WeddingInvitation
//
//  Created by yoseop on 2014. 10. 22..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Couple : NSObject

/*!
 신랑신부 정보
 */
@property (strong) NSString *sinRangName;
@property (strong) NSString *sinRangPhone1;
@property (strong) NSString *sinRangPhone2;
@property (strong) NSString *sinRangPhone3;

@property (strong) NSString *sinBooName;
@property (strong) NSString *sinBooPhone1;
@property (strong) NSString *sinBooPhone2;
@property (strong) NSString *sinBooPhone3;

@property (strong) NSString *year;
@property (strong) NSString *month;
@property (strong) NSString *day;

@property (strong) NSString *address;

/*!
 신랑신부 청첩장
 */
@property (strong) UIImage *invitation;

/*!
 신랑신부 웨딩사진
 */
@property (strong) NSArray *weddingPictures;

+(Couple *)sharedInstance;

@end
