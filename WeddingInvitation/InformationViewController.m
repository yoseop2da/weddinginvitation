//
//  UIInformationViewController.m
//  WeddingInvitation
//
//  Created by yoseop on 2014. 10. 22..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "InformationViewController.h"
#import "InformationTableViewCell.h"

@interface InformationViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *sinRangTextField;
@property (weak, nonatomic) IBOutlet UITextField *sinBooTextField;
@property (weak, nonatomic) IBOutlet UITextField *yearTextField;
@property (weak, nonatomic) IBOutlet UITextField *monthTextField;
@property (weak, nonatomic) IBOutlet UITextField *dayTextField;
@property (weak, nonatomic) IBOutlet UITextField *jangSoTextField;

@property (weak, nonatomic) IBOutlet UITextField *sinRangPhone1;
@property (weak, nonatomic) IBOutlet UITextField *sinRangPhone2;
@property (weak, nonatomic) IBOutlet UITextField *sinRangPhone3;

@property (weak, nonatomic) IBOutlet UITextField *sinBooPhone1;
@property (weak, nonatomic) IBOutlet UITextField *sinBooPhone2;
@property (weak, nonatomic) IBOutlet UITextField *sinBooPhone3;

@property (weak, nonatomic) IBOutlet InformationTableViewCell *datePickerCell;

@end

@implementation InformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"정보";
    [self.sinRangTextField becomeFirstResponder];
    
    [self testSetting];
}

- (void)testSetting
{
    [Couple sharedInstance].sinRangName = @"장동건";
    [Couple sharedInstance].sinRangPhone1 = @"010";
    [Couple sharedInstance].sinRangPhone2 = @"9999";
    [Couple sharedInstance].sinRangPhone3 = @"1234";
    
    [Couple sharedInstance].sinBooName = @"고소영";
    [Couple sharedInstance].sinBooPhone1 = @"010";
    [Couple sharedInstance].sinBooPhone2 = @"1234";
    [Couple sharedInstance].sinBooPhone3 = @"1010";
    
    [Couple sharedInstance].year = @"2014";
    [Couple sharedInstance].month = @"01";
    [Couple sharedInstance].day = @"12";
    
    [Couple sharedInstance].address = @"서울시 마포구 서교동 신형빌딩 2층 앱포스터 웨딩홀 톡송홀";
}

- (void)viewWillAppear:(BOOL)animated
{
    self.sinRangTextField.text = [Couple sharedInstance].sinRangName;
    self.sinRangPhone1.text = [Couple sharedInstance].sinRangPhone1;
    self.sinRangPhone2.text = [Couple sharedInstance].sinRangPhone2;
    self.sinRangPhone3.text = [Couple sharedInstance].sinRangPhone3;
    
    self.sinBooTextField.text = [Couple sharedInstance].sinBooName;
    self.sinBooPhone1.text = [Couple sharedInstance].sinBooPhone1;
    self.sinBooPhone2.text = [Couple sharedInstance].sinBooPhone2;
    self.sinBooPhone3.text = [Couple sharedInstance].sinBooPhone3;
    
    self.yearTextField.text = [Couple sharedInstance].year;
    self.monthTextField.text = [Couple sharedInstance].month;
    self.dayTextField.text = [Couple sharedInstance].day;
    self.jangSoTextField.text = [Couple sharedInstance].address;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeButtonTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)nextButtonTouched:(id)sender
{
    if(![self isThereBlankField]) return;
    if(![self isValid]) return;
    
    [Couple sharedInstance].sinRangName = self.sinRangTextField.text;
    [Couple sharedInstance].sinRangPhone1 = self.sinRangPhone1.text;
    [Couple sharedInstance].sinRangPhone2 = self.sinRangPhone2.text;
    [Couple sharedInstance].sinRangPhone3 = self.sinRangPhone3.text;
    
    [Couple sharedInstance].sinBooName = self.sinBooTextField.text;
    [Couple sharedInstance].sinBooPhone1 = self.sinBooPhone1.text;
    [Couple sharedInstance].sinBooPhone2 = self.sinBooPhone2.text;
    [Couple sharedInstance].sinBooPhone3 = self.sinBooPhone3.text;
    
    [Couple sharedInstance].year = self.yearTextField.text;
    [Couple sharedInstance].month = self.monthTextField.text;
    [Couple sharedInstance].day = self.dayTextField.text;
    
    [Couple sharedInstance].address = self.jangSoTextField.text;
    
    UIViewController *invitationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"InvitationViewController"];
    [self.navigationController pushViewController:invitationViewController animated:YES];
}



- (BOOL)isValid
{
    NSString *message;
    if ([self.dayTextField.text integerValue] > 31) {
        message = @"일자 정보가 잘못되었습니다.";
        [self showAlertWithMessage:message textField:self.dayTextField];
        return  NO;
    }
    
    if ([self.monthTextField.text integerValue] > 12) {
        message = @"일자 정보가 잘못되었습니다.";
        [self showAlertWithMessage:message textField:self.monthTextField];
        return  NO;
    }
    
    return YES;
}

- (BOOL)isThereBlankField
{
    NSString *message;
    
    if( self.sinRangTextField.text.length == 0 )
    {
        message = @"신랑 정보가 비어있습니다.";
        [self showAlertWithMessage:message textField:self.sinRangTextField];
        return NO;
    };
    if( self.sinRangPhone1.text.length == 0 )
    {
        message = @"신랑 연락처 정보가 비어있습니다.";
        [self showAlertWithMessage:message textField:self.sinRangPhone1];
        return NO;
    };
    if( self.sinRangPhone2.text.length == 0 )
    {
        message = @"신랑 연락처 정보가 비어있습니다.";
        [self showAlertWithMessage:message textField:self.sinRangPhone2];
        return NO;
    };
    if( self.sinRangPhone3.text.length == 0 )
    {
        message = @"신랑 연락처 정보가 비어있습니다.";
        [self showAlertWithMessage:message textField:self.sinRangPhone3];
        return NO;
    };
    
    if( self.sinBooTextField.text.length == 0 )
    {
        message = @"신부 정보가 비어있습니다.";
        [self showAlertWithMessage:message textField:self.sinBooTextField];
        return NO;
    };
    if( self.sinBooPhone1.text.length == 0 )
    {
        message = @"신부 연락처 정보가 비어있습니다.";
        [self showAlertWithMessage:message textField:self.sinBooPhone1];
        return NO;
    };
    if( self.sinBooPhone2.text.length == 0 )
    {
        message = @"신부 연락처 정보가 비어있습니다.";
        [self showAlertWithMessage:message textField:self.sinBooPhone2];
        return NO;
    };
    if( self.sinBooPhone3.text.length == 0 )
    {
        message = @"신부 연락처 정보가 비어있습니다.";
        [self showAlertWithMessage:message textField:self.sinBooPhone3];
        return NO;
    };
    
    if( self.yearTextField.text.length == 0 )
    {
        message = @"년도 정보가 비어있습니다.";
        [self showAlertWithMessage:message textField:self.yearTextField];
        return NO;
    };
    if( self.monthTextField.text.length == 0 )
    {
        message = @"월 정보가 비어있습니다.";
        [self showAlertWithMessage:message textField:self.monthTextField];
        return NO;
    };
    if( self.dayTextField.text.length == 0 )
    {
        message = @"일 정보가 비어있습니다.";
        [self showAlertWithMessage:message textField:self.dayTextField];
        return NO;
    };
    
    if( self.jangSoTextField.text.length == 0 )
    {
        message = @"장소 정보가 비어있습니다.";
        [self showAlertWithMessage:message textField:self.jangSoTextField];
        return NO;
    };
    
    return YES;
}

- (void)showAlertWithMessage:(NSString *)message textField:(UITextField *)textfield
{
    ButtonItem *okButton = [ButtonItem itemWithLabel:@"확인"];
    ButtonItem *cancelButton = [ButtonItem itemWithLabel:@"취소"];
    
    [[[UIAlertView alloc] initWithTitle:@"Information" message:message cancelButtonItem:cancelButton otherButtonItems:okButton, nil] show];
    [textfield becomeFirstResponder];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 10;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    return 44;
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
#define IPHONE4 480
    if (iOSDeviceScreenSize.height == IPHONE4) {
        return 30;
    }
    else
    {
        return 44;
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([self.sinRangTextField isFirstResponder]){
        [self.sinBooTextField becomeFirstResponder];
    }
    else if([self.sinBooTextField isFirstResponder]){
        [self.yearTextField becomeFirstResponder];
    }
    else if([self.yearTextField isFirstResponder]){
        [self.monthTextField becomeFirstResponder];
    }
    else if([self.monthTextField isFirstResponder]){
        [self.dayTextField becomeFirstResponder];
    }
    else if([self.dayTextField isFirstResponder]){
        [self.jangSoTextField becomeFirstResponder];
    }
    else if([self.jangSoTextField isFirstResponder]){
        [self.sinRangPhone1 becomeFirstResponder];
    }else{
//        [self saveButtonTouched:nil];
    }
    
    return YES;
}

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([self.yearTextField isFirstResponder]){
        return [self textFieldValidateWithTextField:self.yearTextField newString:string validByte:4];
    }
    else if([self.monthTextField isFirstResponder]){
        return [self textFieldValidateWithTextField:self.monthTextField newString:string validByte:2];
    }
    
    else if([self.dayTextField isFirstResponder]){
        return [self textFieldValidateWithTextField:self.dayTextField newString:string validByte:2];
    }
    
    
    // 신랑 연락처
    else if([self.sinRangPhone1 isFirstResponder]){
        return [self textFieldValidateWithTextField:self.sinRangPhone1 newString:string validByte:3];
    }
    else if([self.sinRangPhone2 isFirstResponder]){
        return [self textFieldValidateWithTextField:self.sinRangPhone2 newString:string validByte:4];
    }
    else if([self.sinRangPhone3 isFirstResponder]){
        return [self textFieldValidateWithTextField:self.sinRangPhone3 newString:string validByte:4];
    }
    
    // 신부 연락처
    else if([self.sinBooPhone1 isFirstResponder]){
        return [self textFieldValidateWithTextField:self.sinBooPhone1 newString:string validByte:3];
    }
    else if([self.sinBooPhone2 isFirstResponder]){
        return [self textFieldValidateWithTextField:self.sinBooPhone2 newString:string validByte:4];
    }
    else if([self.sinBooPhone3 isFirstResponder]){
        return [self textFieldValidateWithTextField:self.sinBooPhone3 newString:string validByte:4];
    }
    
    else{
        
//        NSLog(@"제한과 상관 없는 필드");
    }

    return YES;
}

- (BOOL)textFieldValidateWithTextField:(UITextField *)textField newString:(NSString *)string validByte:(int)validByte
{
    NSUInteger bytes = [textField.text lengthOfBytesUsingEncoding:NSUTF8StringEncoding]+1;
    if (bytes <= validByte) {
        return YES;
    }else{
        if (string.length > 0) {
            return NO;
        }else{
            return YES;
        }
    }
}

@end
