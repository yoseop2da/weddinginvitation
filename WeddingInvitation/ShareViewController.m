//
//  ShareViewController.m
//  WeddingInvitation
//
//  Created by yoseop on 2014. 10. 23..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "ShareViewController.h"
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "DCKakaoActivity.h"
#import "LINEActivity.h"

#define SHARED_IMAGE_WIDTH 600 // [UIScreen mainScreen].bounds.size.width
#define DEFAULT_HEIGHT_SPACE_BETWEEN_ITEMS 10
#define DEFAULT_LEFT_SPACE 150 // 10

@interface ShareViewController ()<MFMessageComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *preView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong) UIImage *captureImage;
@end


@implementation ShareViewController
{
    BOOL _isTouchedPreview;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.title = @"공유";
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"취소" style:UIBarButtonItemStylePlain target:self action:@selector(close)];
    //▼
    UIBarButtonItem *preViewItem = [[UIBarButtonItem alloc] initWithTitle:@"크게보기▼" style:UIBarButtonItemStylePlain target:self action:@selector(showPreview)];
    UIBarButtonItem *shareItem = [[UIBarButtonItem alloc] initWithTitle:@"공유하기" style:UIBarButtonItemStylePlain target:self action:@selector(sendInviTation)];
    self.navigationItem.rightBarButtonItems = @[shareItem, preViewItem];
    
    [self scrollViewSetting];
    [self showPreview];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)scrollViewSetting
{
    // 시작위치 0,0
    self.scrollView.contentOffset = CGPointMake(0, 0);
    
    // 내부의 추가된 뷰는 제거하여 초기화
    for (UIView *v in self.scrollView.subviews) {
        [v removeFromSuperview];
    }
    
    CGRect workingFrame = CGRectMake(0, 0, SHARED_IMAGE_WIDTH, SHARED_IMAGE_WIDTH);
    
    workingFrame = [self setInformationWithFrame:workingFrame];
    workingFrame = [self setWeddingAlbumsWithFrame:workingFrame];
    workingFrame = [self setInvitationWithFrame:workingFrame];
    
    self.scrollView.contentSize = CGSizeMake(workingFrame.size.width,workingFrame.origin.y);
}

- (CGRect)setInformationWithFrame:(CGRect)frame
{
    CGRect workingFrame = frame;

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, SHARED_IMAGE_WIDTH, 30)];
    titleLabel.text = [NSString stringWithFormat:@"%@ ♥ %@",[Couple sharedInstance].sinRangName ,[Couple sharedInstance].sinBooName];//♡
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont boldSystemFontOfSize:35.0f];
    
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, titleLabel.frame.size.height + 20, SHARED_IMAGE_WIDTH, 60)];
    descriptionLabel.text = @"세상에서 가장 아름다운\n결혼식에 당신을 초대합니다.";
    descriptionLabel.textAlignment = NSTextAlignmentCenter;
    descriptionLabel.font = [UIFont boldSystemFontOfSize:19.0f];
    descriptionLabel.numberOfLines = 0;
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(DEFAULT_LEFT_SPACE, descriptionLabel.frame.origin.y + descriptionLabel.frame.size.height, SHARED_IMAGE_WIDTH-20, 30)];
    NSString *date = [NSString stringWithFormat:@"%@년 %@월 %@일",[Couple sharedInstance].year,[Couple sharedInstance].month,[Couple sharedInstance].day];
    dateLabel.text = [NSString stringWithFormat:@"일시 : %@",date];
    dateLabel.textAlignment = NSTextAlignmentLeft;
    dateLabel.font = [UIFont boldSystemFontOfSize:19.0f];
    
    UILabel *addressTitle = [[UILabel alloc] initWithFrame:CGRectMake(DEFAULT_LEFT_SPACE, dateLabel.frame.origin.y + dateLabel.frame.size.height, 48, 30)];
    addressTitle.text = @"장소 : ";
    addressTitle.textAlignment = NSTextAlignmentLeft;
    addressTitle.font = [UIFont boldSystemFontOfSize:19.0f];

    NSString *address = [Couple sharedInstance].address;
    UILabel *addressLabel;
    if (address.length < 16) {
        addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(addressTitle.frame.origin.x + addressTitle.frame.size.width, dateLabel.frame.origin.y + dateLabel.frame.size.height, SHARED_IMAGE_WIDTH-(addressTitle.frame.origin.x + addressTitle.frame.size.width)-10, 30)];
    }else{
        addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(addressTitle.frame.origin.x + addressTitle.frame.size.width, dateLabel.frame.origin.y + dateLabel.frame.size.height, SHARED_IMAGE_WIDTH-(addressTitle.frame.origin.x + addressTitle.frame.size.width)-DEFAULT_LEFT_SPACE, 50)];
        addressLabel.numberOfLines = 0;
    }
    addressLabel.text = [NSString stringWithFormat:@"%@",address];
    addressLabel.textAlignment = NSTextAlignmentLeft;
    addressLabel.font = [UIFont boldSystemFontOfSize:19.0f];
    
    UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(DEFAULT_LEFT_SPACE, addressLabel.frame.origin.y + addressLabel.frame.size.height, SHARED_IMAGE_WIDTH-20, 60)];
    NSString *sinRangPhone = [NSString stringWithFormat:@"%@-%@-%@",[Couple sharedInstance].sinRangPhone1,[Couple sharedInstance].sinRangPhone2,[Couple sharedInstance].sinRangPhone3];
    NSString *sinBooPhone = [NSString stringWithFormat:@"%@-%@-%@",[Couple sharedInstance].sinBooPhone1,[Couple sharedInstance].sinBooPhone2,[Couple sharedInstance].sinBooPhone3];
    phoneLabel.text = [NSString stringWithFormat:@"신랑 : %@\n신부 : %@",sinRangPhone,sinBooPhone];
    phoneLabel.textAlignment = NSTextAlignmentLeft;
    phoneLabel.font = [UIFont boldSystemFontOfSize:19.0f];
    phoneLabel.numberOfLines = 0;

    
    CGFloat titleLabelHeight = CGRectGetHeight(titleLabel.frame);
    CGFloat descriptionLabelHeight = CGRectGetHeight(descriptionLabel.frame);
    CGFloat dateLabelHeight = CGRectGetHeight(dateLabel.frame);
    CGFloat addressLabelHeight = CGRectGetHeight(addressLabel.frame);
    CGFloat phoneLabelHeight = CGRectGetHeight(phoneLabel.frame);
    
    
    
    workingFrame.size.height = titleLabelHeight + descriptionLabelHeight + dateLabelHeight + addressLabelHeight + phoneLabelHeight + 40;
    
    UIImageView *informationView = [[UIImageView alloc] initWithFrame:frame];
    informationView.image = [UIImage imageNamed:@"wedding2"];
    informationView.contentMode = UIViewContentModeScaleAspectFill;
    
    [informationView addSubview:titleLabel];
    [informationView addSubview:descriptionLabel];
    [informationView addSubview:dateLabel];
    [informationView addSubview:addressTitle];
    [informationView addSubview:addressLabel];
    [informationView addSubview:phoneLabel];
    
    [self.scrollView addSubview:informationView];
    
    workingFrame.origin.y = workingFrame.origin.y + workingFrame.size.height;
    
    return workingFrame;
}

- (CGRect)setWeddingAlbumsWithFrame:(CGRect)frame
{
    CGRect workingFrame = frame;
    // 이미지를 스크롤 뷰에 추가하기
    
    workingFrame.origin.y = workingFrame.origin.y + DEFAULT_HEIGHT_SPACE_BETWEEN_ITEMS;
    workingFrame.size.height = DEFAULT_HEIGHT_SPACE_BETWEEN_ITEMS;
    UIView *spaceView = [[UIView alloc] initWithFrame:workingFrame];
    spaceView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:spaceView];
    workingFrame.origin.y = workingFrame.origin.y + workingFrame.size.height;
    
    for (UIImage *image in [Couple sharedInstance].weddingPictures) {
        
        // 이미지 뷰 생성
        workingFrame.size.height = ( image.size.height * SHARED_IMAGE_WIDTH ) / image.size.width;
        
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:workingFrame];//Image:image];
        imageview.contentMode = UIViewContentModeScaleAspectFit;
        imageview.image = image;
        
        // 스크롤뷰에 이미지뷰 추가
        [self.scrollView addSubview:imageview];
        
        // 다음 이미지를 위해 origin 위치를 화면만큼 이동시키기.
        workingFrame.origin.y = workingFrame.origin.y + workingFrame.size.height + DEFAULT_HEIGHT_SPACE_BETWEEN_ITEMS;
    }
    
    return workingFrame;
}

- (CGRect)setInvitationWithFrame:(CGRect)frame
{
    CGRect workingFrame = frame;
    UIImage *invitation = [Couple sharedInstance].invitation;
    
    if (invitation) {
        
        // 이미지를 스크롤 뷰에 추가하기
        workingFrame.origin.y = workingFrame.origin.y+3;
        workingFrame.size.height = 3;
        UIView *spaceView = [[UIView alloc] initWithFrame:workingFrame];
        spaceView.backgroundColor = [UIColor whiteColor];
        [self.scrollView addSubview:spaceView];
        
        workingFrame.origin.y = workingFrame.origin.y + workingFrame.size.height;
        
        // 이미지 뷰 생성
        workingFrame.size.height = ( invitation.size.height * SHARED_IMAGE_WIDTH ) / invitation.size.width;
        
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:workingFrame];//Image:image];
        imageview.contentMode = UIViewContentModeScaleAspectFit;
        imageview.image = invitation;
        
        // 스크롤뷰에 이미지뷰 추가
        [self.scrollView addSubview:imageview];
        
        // 다음 이미지를 위해 origin 위치를 화면만큼 이동시키기.
        workingFrame.origin.y = workingFrame.origin.y + workingFrame.size.height + DEFAULT_HEIGHT_SPACE_BETWEEN_ITEMS;
    }
    
    return workingFrame;
}

- (void)getScrollViewScreenShot
{
    UIImage* image = nil;
    
    UIGraphicsBeginImageContext(_scrollView.contentSize);
    {
        CGPoint savedContentOffset = _scrollView.contentOffset;
        CGRect savedFrame = _scrollView.frame;
        
        _scrollView.contentOffset = CGPointZero;
        _scrollView.frame = CGRectMake(0, 0, _scrollView.contentSize.width, _scrollView.contentSize.height);
        
        [_scrollView.layer renderInContext: UIGraphicsGetCurrentContext()];
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        _scrollView.contentOffset = savedContentOffset;
        _scrollView.frame = savedFrame;
    }
    UIGraphicsEndImageContext();
    
    self.captureImage = image;
}

- (void)showPreview;
{
    if(_isTouchedPreview){
        
        // 프리뷰 닫기
        [self getScrollViewScreenShot];
        self.preView.hidden = YES;
        self.scrollView.userInteractionEnabled = YES;
        UIBarButtonItem *preViewItem = [[UIBarButtonItem alloc] initWithTitle:@"전체보기▲" style:UIBarButtonItemStylePlain target:self action:@selector( showPreview)];
        UIBarButtonItem *shareItem = [[UIBarButtonItem alloc] initWithTitle:@"공유하기" style:UIBarButtonItemStylePlain target:self action:@selector( sendInviTation)];
        self.navigationItem.rightBarButtonItems = @[shareItem, preViewItem];
        
    }else{
        
        // 프리뷰 열기
        [self getScrollViewScreenShot];
        self.preView.hidden = NO;
        self.scrollView.userInteractionEnabled = NO;
        UIBarButtonItem *preViewItem = [[UIBarButtonItem alloc] initWithTitle:@"크게보기▼" style:UIBarButtonItemStylePlain target:self action:@selector(showPreview)];
        UIBarButtonItem *shareItem = [[UIBarButtonItem alloc] initWithTitle:@"공유하기" style:UIBarButtonItemStylePlain target:self action:@selector( sendInviTation)];
        self.navigationItem.rightBarButtonItems = @[shareItem, preViewItem];
    }
    self.preView.center = self.scrollView.center;
    self.preView.image = self.captureImage;
    
    _isTouchedPreview = !_isTouchedPreview;
}

- (void)openShareActivity
{
//    DCKakaoActivity *kakao = [[DCKakaoActivity alloc] init];
//    LINEActivity *line = [[LINEActivity alloc] init];
    NSString *textToShare = [self weddingMessage];
    UIImage *imageToShare = self.captureImage;//[UIImage imageNamed:@"invitation"];
    NSArray *itemsToShare = @[textToShare, imageToShare];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:@[]];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact]; //UIActivityTypeSaveToCameraRoll //or whichever you don't need
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (void)sendMessage
{
    MFMessageComposeViewController *controller = [MFMessageComposeViewController new];

    if([MFMessageComposeViewController canSendText]){
        controller.body = [self weddingMessage];
//        controller.recipients = [NSArray arrayWithObjects:@"(010)8999-7677", nil];
        controller.messageComposeDelegate = self;
        UIImage *image = self.captureImage;
        NSData *data = UIImagePNGRepresentation(image);
        [controller addAttachmentData:data typeIdentifier:(NSString* )kUTTypePNG filename:@"wedding.mp4"];

        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)sendInviTation
{
    [self getScrollViewScreenShot];
    
    [self openShareActivity];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (NSString *)weddingMessage
{
    NSString *sinrang = [Couple sharedInstance].sinRangName;
    NSString *sinRangPhone = [NSString stringWithFormat:@"%@-%@-%@",[Couple sharedInstance].sinRangPhone1,[Couple sharedInstance].sinRangPhone2,[Couple sharedInstance].sinRangPhone3];
    NSString *sinboo = [Couple sharedInstance].sinBooName;
    NSString *sinBooPhone = [NSString stringWithFormat:@"%@-%@-%@",[Couple sharedInstance].sinBooPhone1,[Couple sharedInstance].sinBooPhone2,[Couple sharedInstance].sinBooPhone3];
    NSString *date = [NSString stringWithFormat:@"%@년 %@월 %@일",[Couple sharedInstance].year,[Couple sharedInstance].month,[Couple sharedInstance].day];
    NSString *address = [Couple sharedInstance].address;
    
    NSString *message = [NSString stringWithFormat:@"세상에서 가장 아름다운\n결혼식에 당신을 초대합니다.\n 신랑 : %@(%@)\n 신부 : %@(%@)\n 일시 : %@\n 장소 : %@",sinrang,sinRangPhone,sinboo,sinBooPhone,date,address];
    return message;
    
}
@end
