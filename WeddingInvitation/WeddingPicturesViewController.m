//
//  WeddingPicturesViewController.m
//  WeddingInvitation
//
//  Created by yoseop on 2014. 10. 21..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "WeddingPicturesViewController.h"
//#import <MobileCoreServices/UTCoreTypes.h>
//#import <AssetsLibrary/AssetsLibrary.h>
//#import "ELCImagePickerController.h"
//#import "ELCAlbumPickerController.h"
//#import "ELCImagePickerController.h"
//#import "ELCAssetTablePicker.h"

@interface WeddingPicturesViewController ()<UIImagePickerControllerDelegate, ELCImagePickerControllerDelegate>
@property (nonatomic, strong) ALAssetsLibrary *specialLibrary;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, copy) NSArray *weddingPictures;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@end

@implementation WeddingPicturesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"웨딩 사진";
    
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"다음으로" style:UIBarButtonItemStylePlain target:self action:@selector(nextButtonTouched)];
    UIBarButtonItem *cameraButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addButtonTouched:)];
    self.navigationItem.rightBarButtonItems = @[rightButtonItem,cameraButtonItem];
    
    self.weddingPictures = [Couple sharedInstance].weddingPictures;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.weddingPictures) {
        [self setImageToScrollView];
        self.pageControl.hidden = NO;
        self.pageControl.numberOfPages = self.weddingPictures.count;
    }else{
        self.pageControl.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setImageToScrollView
{
    // 시작위치 0,0
    self.scrollView.contentOffset = CGPointMake(0, 0);

    // 내부의 추가된 뷰는 제거하여 초기화
    for (UIView *v in self.scrollView.subviews) {
        [v removeFromSuperview];
    }
    
    
#define WIDTH [UIScreen mainScreen].bounds.size.width - 40
    //280 // (self.scrollView.frame.size.width) //
#define HEIGHT [UIScreen mainScreen].bounds.size.height - 120
    //364 // (self.scrollView.frame.size.height) //
    
    // 추가할 새로운 뷰의 위치 선정.
    CGRect workingFrame = CGRectMake(0, 0, WIDTH, HEIGHT);

    // 이미지를 스크롤 뷰에 추가하기
    for (UIImage *image in self.weddingPictures) {
        
        // 이미지 뷰 생성
        UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
        imageview.contentMode = UIViewContentModeScaleAspectFit;
        imageview.frame = workingFrame;
        
        // 스크롤뷰에 이미지뷰 추가
        [self.scrollView addSubview:imageview];
        
        // 다음 이미지를 위해 origin 위치를 화면만큼 이동시키기.
        workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
    }
    
    self.scrollView.pagingEnabled = YES;
    self.scrollView.contentSize = CGSizeMake(workingFrame.origin.x, workingFrame.size.height);
}

- (void)addButtonTouched:(id)sender {
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
    
    elcPicker.maximumImagesCount = 4;
    elcPicker.returnsOriginalImage = YES;
    elcPicker.returnsImage = YES;
    elcPicker.onOrder = YES;
    elcPicker.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie];
    
    elcPicker.imagePickerDelegate = self;
    
    [self presentViewController:elcPicker animated:YES completion:nil];
    
}

- (IBAction)closedButtonTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)nextButtonTouched
{
    if (self.weddingPictures) {
        [Couple sharedInstance].weddingPictures = self.weddingPictures;
        
        UIViewController *shareViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareViewController"];
        [self.navigationController pushViewController:shareViewController animated:YES];
    }else{
        [self showAlertWithMessage:@"웨딩 사진이 비어있습니다."];
    }
}

- (void)showAlertWithMessage:(NSString *)message
{
    ButtonItem *okButton = [ButtonItem itemWithLabel:@"웨딩 사진 추가하기"];
    [okButton setAction:^{
        [self addButtonTouched:nil];
    }];
    ButtonItem *cancelButton = [ButtonItem itemWithLabel:@"Skip"];
    [cancelButton setAction:^{
        UIViewController *shareViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareViewController"];
        [self.navigationController pushViewController:shareViewController animated:YES];
    }];
    
    [[[UIAlertView alloc] initWithTitle:self.title message:message cancelButtonItem:cancelButton otherButtonItems:okButton, nil] show];
}

#pragma mark ELCImagePickerControllerDelegate Methods

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
    for (NSDictionary *dict in info) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                
                UIImage *image = dict[UIImagePickerControllerOriginalImage];//[dict objectForKey:UIImagePickerControllerOriginalImage];
                [images addObject:image];
            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        }
    }
    self.pageControl.hidden = NO;
    self.pageControl.numberOfPages = images.count;
    
    self.weddingPictures = images;
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    static NSInteger previousPage = 0;
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    if (previousPage != page) {
        previousPage = page;
        
        [self.pageControl setCurrentPage:page];
    }
}

@end