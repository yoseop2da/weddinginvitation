//
//  Couple.m
//  WeddingInvitation
//
//  Created by yoseop on 2014. 10. 22..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "Couple.h"

@implementation Couple

+(Couple *)sharedInstance
{
    static dispatch_once_t predicate = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&predicate, ^{
        sharedObject = [self new];
    });
    return sharedObject;
}
@end
